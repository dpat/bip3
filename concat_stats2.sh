#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

###############################################################################

###############################################################################

COMMENTBLOCK
###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   Main
Purpose:    Concatenate data from subjects' stats directories into
            all_stats_endpoints.tsv and all_stats_tracts.tsv in the output directory
Input:      *endpoint* and *tract* tsv files in each subject's stats directory
            and empty all_stats files created by concat_stats1.sh
Output:     ${output_dir}/all_stats_endpoints.tsv and
            ${output_dir}/all_stats_tracts.tsv

COMMENTBLOCK

Main ()
{
# Check that the output stats dir exists
CheckDir ${output_dir}/${subj}/stats "stats dir for ${subj} exists" "stats dir for ${subj} does not exist, moving on"
stats_dir_exist=${dir_exist} # Create a variable to hold True or False
# If the subject stats dir exists:
if [ "${stats_dir_exist}" = "True" ]; then
  endpoint_exists=$(FileExists ${output_dir}/${subj}/stats "stats_endpoint*")
  # use tail to move to the 2nd line of output for file before concatenation
  if [ "${endpoint_exists}" = "0" ]; then
    for end_files in ${output_dir}/${subj}/stats/*endpoint*; do
      tail -q -n +2 ${end_files} >> ${output_dir}/all_stats_endpoints.tsv
    done
  fi

  tract_exists=$(FileExists ${output_dir}/${subj}/stats "stats_tract*")
  if [ "${tract_exists}" = "0"  ]; then
    for tract_files in ${output_dir}/${subj}/stats/*tract*; do
      tail -q -n +2 ${tract_files} >> ${output_dir}/all_stats_tracts.tsv
    done
  fi
fi
}
#==============================================================================

source ${BIP_HOME}/bip_functions.sh
# We pass 2 arguments from run.py: the subjectnum and output_dir
subjectnum=$1
subj=sub-${subjectnum}
output_dir=$2 #this should be derivatives/bip

###################

if [ $# -lt 1 ]
then
  HelpMessage
  exit 1
fi

### Sadly, the code below works at each subject, deleting the results repeatedly.
### For now it is commented out 
# Check for the existence of the concatenated output files (endpoints and tracts),
# we'll remove and replace these if they are there to prevent duplicate entries.
# Create a variable to hold "0" if true, "1" otherwise
# all_endpoints_exists=$(FileExists ${output_dir} "all_stats_endpoints.tsv")

# If the file exists, inform the user and remove it
# if [ "${all_endpoints_exists}" = "0"  ]; then
#   echo "removing and replacing existing ${output_dir}/all_stats_endpoints.tsv"
#   rm ${output_dir}/all_stats_endpoints.tsv
# fi

# # Create a variable to hold "0" if true, "1" otherwise
# all_tracts_exists=$(FileExists ${output_dir} "all_stats_tracts.tsv")

# # If the file exists, inform the user and remove it
# if [ "${all_tracts_exists}" = "0"  ]; then
#   echo "removing and replacing existing ${output_dir}/all_stats_tracts.tsv"
#   rm ${output_dir}/all_stats_tracts.tsv
# fi  

Main ${subjectnum} ${output_dir}
