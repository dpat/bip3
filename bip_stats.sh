#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   Today is 2/26/12. It is now 5:20 PM
----------------------------  DEPENDENCIES  -----------------------------------
The results of bip.sh
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
Generate standard space endpoint masks, tract mask,and tract. Statistics for endpoints: volume, Center of Gravity; and tract: volume, scalar dwi values.
===============================================================================
----------------------------  INPUT  ------------------------------------------
A directory containing the results of running bip.
===============================================================================
----------------------------  OUTPUT  -----------------------------------------
-Data in Access/access_endpoint_bip.txt. This contains volume, cog and dti values
for the endpoints.
-Data in Access/access_tract_bip.txt. This contains volume and dti values
for the tract extracted from the highest numbered Iteration directory (B if
there are an equal number of Iterations for A and B)
===============================================================================
----------------------------  CALLS   -----------------------------------------
B02MNIMaskWarp and B02MNIWarp from bip_functions.sh
###############################################################################

COMMENTBLOCK

###############################################################################
############################  DEFINE FUNCTIONS  ##################################

#==============================================================================

: <<COMMENTBLOCK

Function:   Diff2Stand
Purpose:    Convert final diffusion space output of bip to standard space
Input:      native diffusion space *final* images in ${tract_dir}
Output:     standard space mask images for endpoints and tract,
            standard space path image
Calls:      B02MNIMaskWarp and B02MNIWarp from bip_functions.sh

COMMENTBLOCK

Diff2Stand ()
{
  # roi_A mask in MNI space
  if [[ ! -e ${tract_dir}/roi_A_${tract}_${subj}_final_stand.nii.gz ]]; then
    B02MNIMaskWarp ${tract_dir}/roi_A_${tract}_${subj}_final ${tract_dir}/roi_A_${tract}_${subj}_final_stand
  fi
  # roi_B mask in MNI space
  if [[ ! -e ${tract_dir}/roi_B_${tract}_${subj}_final_stand.nii.gz ]]; then
    B02MNIMaskWarp ${tract_dir}/roi_B_${tract}_${subj}_final ${tract_dir}/roi_B_${tract}_${subj}_final_stand
  fi

  # average thresholded truncated tract mask in MNI space
  if [[ ! -e ${tract_dir}/tract_${tract}_${subj}_bin_final_stand.nii.gz ]]; then
    B02MNIMaskWarp ${tract_dir}/tract_${tract}_${subj}_bin_final ${tract_dir}/tract_${tract}_${subj}_final_stand
  fi

  # average thresholded truncated tract in MNI space
  if [[ ! -e ${tract_dir}/tract_${tract}_${subj}_final_stand.nii.gz ]]; then
    B02MNIWarp ${tract_dir}/tract_${tract}_${subj}_final  ${tract_dir}/tract_${tract}_${subj}_final_stand
  fi
}

#==============================================================================
: <<COMMENTBLOCK

Function:   GetStatsEnds
Purpose:    Get dwi endpoint statistics
Input:      A tract dir in dwi (created by bip.sh)
Output:     A tsv file: ${output_dir}/${subj}/stats/stats_tract_${subj}.tsv

echo roi_A_arc_l_sub-327_final.nii.gz | cut -d '_' -f 2

COMMENTBLOCK

GetStatsEnds ()
{
  mask=$1

  # Get volume in cubic mm
  vol=$(fslstats ${mask} -V | awk '{printf $2 "\n"}')
  # get the name of the endpoint (A if odd numbered, or B if even numbered)
  base_mask=(basename ${mask})
  echo "base_mask=${base_mask}"
  end=$(echo ${base_mask} | cut -d '_' -f 2)
  # get center of gravity in voxel coordinates
  # all positive values which facilitate comparisons
  COG_x=$(fslstats ${mask} -C | awk '{printf $1 "\n"}')
  COG_y=$(fslstats ${mask} -C | awk '{printf $2 "\n"}')
  COG_z=$(fslstats ${mask} -C | awk '{printf $3 "\n"}')

  printf  "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" ${subj} ${tract} ${side} ${end} ${vol} ${COG_x} ${COG_y} ${COG_z} >> ${output_dir}/${subj}/stats/stats_endpoints_${subj}_${tract}.tsv
}


#==============================================================================
: <<COMMENTBLOCK

Function:   GetStatsTract
Purpose:    Get dwi tract statistics
Input:      A tract dir in dwi (created by bip.sh)
Output:     A tsv file: ${output_dir}/${subj}/stats/stats_tract_${subj}.tsv
COMMENTBLOCK

GetStatsTract ()
{
  if [[ -e ${tract_dir}/tract_${tract}_${subj}_final_stand_bin.nii.gz ]]; then
    # Get volume in cubic mm for the standard space mask image
    vol=$(fslstats ${tract_dir}/tract_${tract}_${subj}_final_stand_bin -V | awk '{printf $2 "\n"}')
    # Get mean tract values for dwi scalar measures.  This is native space
    # as native space will not be interpolated at all, hence the cleanest results.
    md=$(fslstats ${dwi_outdir}/${subj}/dwi/${subj}_MD.nii.gz -k ${tract_dir}/tract_${tract}_${subj}_final -M)
    fa=$(fslstats ${dwi_outdir}/${subj}/dwi/${subj}_FA.nii.gz -k ${tract_dir}/tract_${tract}_${subj}_final -M)
    rd=$(fslstats ${dwi_outdir}/${subj}/dwi/${subj}_RD.nii.gz -k ${tract_dir}/tract_${tract}_${subj}_final -M)
    pd=$(fslstats ${dwi_outdir}/${subj}/dwi/${subj}_L1.nii.gz -k ${tract_dir}/tract_${tract}_${subj}_final -M)
    mo=$(fslstats ${dwi_outdir}/${subj}/dwi/${subj}_MO.nii.gz -k ${tract_dir}/tract_${tract}_${subj}_final -M)

    printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n"  ${subj} ${tract} ${side} ${vol} ${md} ${fa} ${rd} ${pd} ${mo} >> ${output_dir}/${subj}/stats/stats_tract_${subj}_${tract}.tsv
  fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   MkStats
Purpose:    Create stats for standard space images
Input:      MNI space *final* images in ${tract_dir}
Output:     tsv files containing useful endpoint and tract statistics
            for final standard space images
Calls:

COMMENTBLOCK

MkStats ()
{
  # Create the tab-separated-value files to hold the statistics
  # This file contains the tract statistics.  It overwrites previous results to
  # prevent having multiple conflicting results in one file.

  printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "subject" "tract" "side" "volmm2" "MD" "FA" "RD" "PD" "MO" > ${output_dir}/${subj}/stats/stats_tract_${subj}_${tract}.tsv

  printf  "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "subject" "tract" "side" "end" "volmm2" "COG_x" "COG_y" "COG_z" > ${output_dir}/${subj}/stats/stats_endpoints_${subj}_${tract}.tsv

  # Generate the tract statistics
  GetStatsTract
  # Generate the endpoint statistics
  if [[ -e ${tract_dir}/roi_A_${tract}_${subj}_final_stand_bin.nii.gz ]]; then
    GetStatsEnds ${tract_dir}/roi_A_${tract}_${subj}_final_stand_bin
    GetStatsEnds ${tract_dir}/roi_B_${tract}_${subj}_final_stand_bin
  fi
}

#==============================================================================


: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Print relevant help message
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
  if [ $# -lt 2 ]; then
    echo "Usage: $0 <tract> <subjectnum>"
    echo "Example: $0 arc_r 327"
    exit 1
  fi

  echo "================================ "
}



#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run standard space statistics
Input:      tract_bip directory where bip.sh was run
Output:     tract images in standard space
            tract and endpoint stats: stats.txt, statsA.txt and statsB.txt
            endpoint images in standard space (in DTI/Bip_stand)
Calls:
COMMENTBLOCK

Main ()
{
  echo "================="
  echo "bip_stats:"
  Diff2Stand
  MkStats
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

source ${BIP_HOME}/bip_functions.sh
if [ $# -lt 1 ]
then
  HelpMessage
  exit 1
fi

# We pass 4 arguments from run.py: the subjectnum, bids_dir and output_dir, tract
subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
tract=$4

tract_dir=${output_dir}/${subj}/dwi/${tract}_bip
inv=${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf
side=$(echo ${tract} | cut -d '_' -f 2)
dwi_outdir=${bids_dir}/derivatives/fsl_dwi_proc

if [ ! -d "${output_dir}/${subj}/stats" ]; then
  mkdir -p ${output_dir}/${subj}/stats
fi

Main ${subjectnum} ${bids_dir} ${output_dir} ${tract}
