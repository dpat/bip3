#!/usr/bin/env python3
import sys
import os.path
from glob import glob

def main(argv):
    bpath = "{0}".format(argv[1])
    dirstring = glob(bpath)
    
    if (len(dirstring) == 0):
        print ("False")
    else:
        print ("True")

if __name__ == "__main__":
    main(sys.argv)
