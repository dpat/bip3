# BIP3 Docker Image
Dianne Patterson, University of Arizona, RII  
Created Dec 19 2021
Updated Dec 19 2021

## What is BIP
BIP (bidirectional iterative parcellation) uses FSL Bedpost and Probtrackx processing to not only characterize each tract, but also the connected grey matter at each end of a tract:   

Patterson, D. K., Van Petten, C., Beeson, P., Rapcsak, S. Z., & Plante, E. (2014). Bidirectional iterative parcellation of diffusion weighted imaging data: separating cortical regions connected by the arcuate fasciculus and extreme capsule. NeuroImage, 102 Pt 2, 704–716. http://doi.org/10.1016/j.neuroimage.2014.08.032

#### REF subdirectory:
This subdirectory contains non-scripts organized into two subdirectories:
 * LISTS contains lists of defined tracts and rois to facilitate iteration.  
 * IMAGES contains binary image masks for particular tracts (endpoint rois and termination masks).

## TO DO

- Run dtifit on the output of qsiprep
- Make bip3 work with sessions
- Try giraffe to get this going properly, or look at bidlayout stuff we did in intend4
- Run everything in standard space, so there is no conversion at the end.
- Consult https://bitbucket.org/dpat/bipbids/src/master/ for more information.
