#!/bin/bash



#==============================================================================
: <<COMMENTBLOCK

Function:   MkBed
Purpose:    Run bedpostX to prepare dwi data for bip
Input:      Depends on DWICorrect and BBR_DWI
Output:     BedpostX directory

COMMENTBLOCK

MkBed ()
{
	started=$(date "+%Y-%m-%d %T")
	echo "Starting MkBed UTC: ${started}"
	if [ ! -d ${output_dir}/${subj}/dwi.bedpostX ] && [ "${gpu}" = "no" ]; then
		echo "------------------------------------------------"
		echo "running bedpostX, no GPU"
		touch ${output_dir}/${subj}/dwi/GPU_BED_NO
		bedpostx ${output_dir}/${subj}/dwi
	elif [ ! -d ${output_dir}/${subj}/dwi.bedpostX ] && [ "${gpu}" = "yes" ]; then
		echo "------------------------------------------------"
		echo "running bedpostX, with GPU"
		touch ${output_dir}/${subj}/dwi/GPU_BED_YES
		bedpostx_gpu ${output_dir}/${subj}/dwi
	elif 	[ -d ${output_dir}/${subj}/dwi.bedpostX ] && [ -d ${output_dir}/${subj}/dwi.bedpostX/diff_slices ]; then
		echo "BedpostX was interrupted. Restarting from where you left off."
			if [ "${gpu}" = "no" ]; then
				echo "------------------------------------------------"
				echo "running bedpostX with no GPU"
				touch ${output_dir}/${subj}/dwi/GPU_BED_NO
				bedpostx ${output_dir}/${subj}/dwi
			elif [ "${gpu}" = "yes" ]; then
				echo "------------------------------------------------"
				echo "running bedpostX with GPU"
				touch ${output_dir}/${subj}/dwi/GPU_BED_YES
				bedpostx_gpu ${output_dir}/${subj}/dwi
			fi
		else
			echo "BedpostX was already run. Moving on."
	fi
	finished=$(date "+%Y-%m-%d %T")
	echo "MkBed has completed UTC: ${finished}"
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkDTIFIT
Purpose:    Run dtifit to prepare dwi data for bip
Input:      Depends on DWICorrect and BBR_DWI
Output:     Dtifit scalar images

COMMENTBLOCK

MkDTIFIT ()
{
	started=$(date "+%Y-%m-%d %T")
	echo "Starting MkDTIFIT UTC: ${started}"
	if [ ! -e ${output_dir}/${subj}/dwi/${subj}_FA.nii.gz ]; then
		echo "------------------------------------------------"
		echo "DTIFIT--creates FA and misc other files"
		dtifit -k ${output_dir}/${subj}/dwi/data.nii.gz -o ${output_dir}/${subj}/dwi/${subj} -m ${output_dir}/${subj}/dwi/nodif_brain_mask.nii.gz -r \
		${output_dir}/${subj}/dwi/bvecs -b ${output_dir}/${subj}/dwi/bvals --sse -w
		else
			echo "dtifit was already run.  Moving on."
	fi

	if [ ! -e ${output_dir}/${subj}/dwi/${subj}_RD.nii.gz ]; then
		echo "Calculate the radial diffusity image"
		fslmaths ${output_dir}/${subj}/dwi/${subj}_L2 -add ${output_dir}/${subj}/dwi/${subj}_L3 -div 2 ${output_dir}/${subj}/dwi/${subj}_RD -odt input
	fi
	finished=$(date "+%Y-%m-%d %T")
	echo "MkDTIFIT has completed UTC: ${finished}"
}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help

COMMENTBLOCK

HelpMessage ()
{
	echo
	echo "Assuming a bids compliant data directory, and that setup.sh has run successfully"
	echo "run prep.sh with one argument, the subjectnum:"
	echo "e.g., If your subject is sub-327: prep.sh 327"
	echo "defacing, and masking will be generated for you to check."
	echo "you should be in your bids_dir"
	echo
	exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run main functions: dtifit, bedpost, eddy
Input:      A directory created by setup.sh
COMMENTBLOCK

Main ()
{
	CheckFilesPrep
	if [ -e ${bids_dir}/${subj}_error.txt ]; then

	  echo "Critical files are missing or incorrect!"
		echo "prep cannot be run"
		echo "See ${subj}_error.txt"
		echo "If you have corrected the errors, then remove ${subj}_error.txt and try running again."
	  elif [ ! -e ${bids_dir}/${subj}_error.txt ]; then
			echo ""
			echo "Congratulations! No error file generated!"
			echo "Running prep now."
			# Run motion and distoriton correction on dwi image: eddy or eddy_correct
			DWICorrect
			# epi_reg to do boundary based registration of DWI to structural
			BBRDWI
			# dtifit
			MkDTIFIT
			# bedpostx
			MkBed
	fi
}

#==============================================================================
: <<COMMENTBLOCK
This is the main call
COMMENTBLOCK

source ${BIP_HOME}/bip_functions.sh

# We pass 4 arguments from run.py: the subjectnum, bids_dir and output_dir, gpu
subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3 # this will be derivatives/fsl_dwi_proc
gpu=$4
bip_outdir=${bids_dir}/derivatives/bip
anat_outdir=${bids_dir}/derivatives/fsl_anat_proc
dwi_outdir=${bids_dir}/derivatives/fsl_dwi_proc

for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
  esac                                          # End case
done                                            # End for loop

Main ${subjectnum} ${bids_dir} ${output_dir}

#==============================================================================
