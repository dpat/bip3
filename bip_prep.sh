#!/bin/bash

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   Today is 4/9/14. It is now 5:23 PM
----------------------------  DEPENDENCIES  -----------------------------------
This script assumes the dtifit, bedpostx and the registrations (dwi to MNI and
vice versa) have been run.  We also need the GM, WM, CSF segmentation from anat
space registered into dwi space.
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
Prepare a subject directory for running bip scripts:
Generate a csf mask in diffusion space (if it does not exist)
Generate roi masks for each endpoint.
Generate an inverse mask to restrict the path of each tract.
===============================================================================
----------------------------  INPUT  ------------------------------------------
the name of the tract (from the list of options)

===============================================================================
----------------------------  OUTPUT  -----------------------------------------
A diffusion space csf mask

Identify standard space rois that need to be translated into diffusion space.
This will differ by tract.  See MkSublistArray in bip_functions.sh.

Use this call to create a directory for the tract, e.g., ${output_dir}/${subj}/dwi/arc_l_bip
put the masks.txt (contents of array) in the directory.
For each of the named rois (-_csf extension), get it from the BIP_HOME location, register it into diffusion space and put it in ${output_dir}/${subj}/subjectrois/.
* There may be a need to do additional procedures on some standard rois once
they are in diffusion space.  e.g., may need to add some brocas regions together*

An inverse mask for each tract: ${BIP_HOME}/REF/IMAGES/Inverse
+ the csf mask - native space endpoint rois

===============================================================================
----------------------------  CALLS   -----------------------------------------
From other scripts MkSublistArray, MyLog and MNI2B0Mask from bip_functions.sh;
bip_ireg2.sh functions must be run before bip_prep

###############################################################################

COMMENTBLOCK

###############################################################################
############################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   BuildATL_L
Purpose:    Import standard space rois for ATL,
            add and trim them in subject space.
Input:
Output:     ${output_dir}/${subj}/subjectrois/atl_l_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildATL_L ()
{
  Stand2Diff itg_ant_l mtg_ant_l stg_ant_l tp_l pHg_ant_l fusG_ant_l

  fslmaths ${output_dir}/${subj}/subjectrois/itg_ant_l_diff_bin -add ${output_dir}/${subj}/subjectrois/mtg_ant_l_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/stg_ant_l_diff_bin -add ${output_dir}/${subj}/subjectrois/tp_l_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/pHg_ant_l_diff_bin -add ${output_dir}/${subj}/subjectrois/fusG_ant_l_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/atl_l_diff_bin -odt input

  TrimEndpoints atl_l_diff_bin
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildATL_R
Purpose:    Import standard space rois for ATL,
            add and trim them in subject space.
Input:      In subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/atl_r_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildATL_R ()
{
  Stand2Diff itg_ant_r mtg_ant_r stg_ant_r tp_r pHg_ant_r fusG_ant_r

  fslmaths ${output_dir}/${subj}/subjectrois/itg_ant_r_diff_bin -add ${output_dir}/${subj}/subjectrois/mtg_ant_r_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/stg_ant_r_diff_bin -add ${output_dir}/${subj}/subjectrois/tp_r_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/pHg_ant_r_diff_bin -add ${output_dir}/${subj}/subjectrois/fusG_ant_r_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/atl_r_diff_bin -odt input

  TrimEndpoints atl_r_diff_bin
}

#==============================================================================

: <<COMMENTBLOCK

Function:   BuildBrocas_L
Purpose:    Add ba44,45,47 in subject diffusion space to create brocas3
Input:      ba44,45,47 in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/brocas3_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocas_L ()
{
  Stand2Diff ba44_l ba45_l ba47_l

  fslmaths ${output_dir}/${subj}/subjectrois/ba44_l_diff_bin -add ${output_dir}/${subj}/subjectrois/ba45_l_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/ba47_l_diff_bin -bin ${output_dir}/${subj}/subjectrois/brocas3_l_diff_bin -odt input

  TrimEndpoints brocas3_l_diff_bin
}

#==============================================================================

: <<COMMENTBLOCK

Function:   BuildBrocas_R
Purpose:    Add ba44,45,47 in subject diffusion space to create brocas3
Input:      ba44,45,47 in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/brocas3_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocas_R ()
{
  Stand2Diff ba44_r ba45_r ba47_r

  fslmaths ${output_dir}/${subj}/subjectrois/ba44_r_diff_bin -add ${output_dir}/${subj}/subjectrois/ba45_r_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/ba47_r_diff_bin -bin ${output_dir}/${subj}/subjectrois/brocas3_r_diff_bin -odt input

  TrimEndpoints brocas3_r_diff_bin
}


#==============================================================================
<<COMMENTBLOCK

Function:   BuildBrocasClassic_L
Purpose:    Add ba44 and ba45 to create brocas_classic
Input:      Assumes ba44 and ba45 already exist in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/brocas_classic_l_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocasClassic_L ()
{
  Stand2Diff ba44_l ba45_l

  fslmaths ${output_dir}/${subj}/subjectrois/ba44_l_diff_bin -add ${output_dir}/${subj}/subjectrois/ba45_l_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/brocas_classic_l_diff_bin -odt input

  TrimEndpoints brocas_classic_l_diff_bin
}

#==============================================================================

<<COMMENTBLOCK

Function:   BuildBrocasClassic_R
Purpose:    Add ba44 and ba45 to create brocas_classic
Input:      Assumes ba44 and ba45 already exist in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/brocas_classic_r_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildBrocasClassic_R ()
{
  Stand2Diff ba44_r ba45_r

  fslmaths ${output_dir}/${subj}/subjectrois/ba44_r_diff_bin -add ${output_dir}/${subj}/subjectrois/ba45_r_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/brocas_classic_r_diff_bin -odt input

  TrimEndpoints brocas_classic_r_diff_bin
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildDir
Purpose:    Build tract_dir for given tract and put masks.txt file in it
Input:      standard tract names in a string e.g., blah_l blah_r
Output:     ${DWI_DERIV}/${tract}_bip
Calls:      MkSublistArray from bip_functions.sh

COMMENTBLOCK

BuildDir ()
{
  for tract in $@; do
    if [ ! -d  ${tract_dir} ]; then
      MkSublistArray lst_tract_seed_target_masks.txt ${tract}
      mkdir ${tract_dir}
      echo  "${sublist_array[1]}" > ${tract_dir}/masks.txt
      echo  "${sublist_array[2]}" >> ${tract_dir}/masks.txt
    fi
  done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildEndpoints
Purpose:    Build diff space endpoint from standard
Input:      standard endpoints in a string: e.g., ba44_l ba45_l
Output:     ${output_dir}/${subj}/subjectrois/${roi}_diff_bin
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildEndpoints ()
{
  for roi in $@; do
    Stand2Diff ${roi}
    TrimEndpoints ${roi}_diff_bin
  done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildInverse
Purpose:    Build diff space inverse mask from standard, add csf to mask
Input:      standard Inverse masks in a string e.g., blah_l blah_r
Output:     ${output_dir}/${subj}/subjectrois/${tract}_inverse_mask_diff_bin_csf
Note:       In reaction to Cb-Th tract, I have switched the order of steps
            for building the inverse.  Now csf is added last which means it overrides the endpoint masks.  It probably always should have been this way, but has not been an issue
            until now. July 18, 2016.  Added any lesion mask to Inverse as well (March 20, 2020).
Calls:      MkSublistArray and MNI2B0Mask from bip_functions.sh;

COMMENTBLOCK

BuildInverse ()
{
  for tract in $@; do
    if [ ! -e "${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf.nii.gz" ]; then
      MNI2B0MaskWarp ${BIP_HOME}/REF/IMAGES/Inverse/${tract}_inv_mask ${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff
      MkSublistArray lst_tract_seed_target_masks.txt ${tract}
      fslmaths ${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin -sub ${output_dir}/${subj}/subjectrois/${sublist_array[1]} \
      -sub ${output_dir}/${subj}/subjectrois/${sublist_array[2]} -add ${output_dir}/${subj}/subjectrois/${subj}_B0_csfseg_bin \
      -bin ${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf -odt char
      imrm ${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin
      if [ -e ${output_dir}/${subj}/subjectrois/${subj}_B0_lesion_roi_bin.nii.gz ]; then
        fslmaths ${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf -add ${output_dir}/${subj}/subjectrois/${subj}_B0_lesion_roi_bin -bin -bin ${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf -odt char
      fi
    else
      echo "${tract}_inv_mask_diff_bin_csf.nii.gz already exists"
    fi
  done
}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildMdlfPost_L
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion space
            to create mdlf_post
Input:      angular, mtg_post, stg, supramarginal in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/mdlf_post_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildMdlfPost_L ()
{
  Stand2Diff angular_l mtg_post_l stg_post_l supramarginal_l

  fslmaths ${output_dir}/${subj}/subjectrois/angular_l_diff_bin -add ${output_dir}/${subj}/subjectrois/mtg_post_l_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/stg_post_l_diff_bin -add ${output_dir}/${subj}/subjectrois/supramarginal_l_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/mdlf_post_l_diff_bin -odt input

  TrimEndpoints mdlf_post_l_diff_bin

  fslmaths ${output_dir}/${subj}/subjectrois/mdlf_post_l_diff_bin_csf -sub ${output_dir}/${subj}/subjectrois/atl_l_diff_bin_csf \
  -thr 0 -bin ${output_dir}/${subj}/subjectrois/mdlf_post_l_diff_bin_csf -odt input
}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildMdlfPost_R
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion space
            to create mdlf_post
Input:      angular, mtg_post, stg, supramarginal in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/mdlf_post_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildMdlfPost_R ()
{
  Stand2Diff angular_r mtg_post_r stg_post_r supramarginal_r

  fslmaths ${output_dir}/${subj}/subjectrois/angular_r_diff_bin -add ${output_dir}/${subj}/subjectrois/mtg_post_r_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/stg_post_r_diff_bin -add ${output_dir}/${subj}/subjectrois/supramarginal_r_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/mdlf_post_r_diff_bin -odt input

  TrimEndpoints mdlf_post_r_diff_bin

  fslmaths ${output_dir}/${subj}/subjectrois/mdlf_post_r_diff_bin_csf -sub ${output_dir}/${subj}/subjectrois/atl_r_diff_bin_csf \
  -thr 0 -bin ${output_dir}/${subj}/subjectrois/mdlf_post_r_diff_bin_csf -odt input
}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildSlf2Ant_L
Purpose:    Add frontal_mid and ba6and8 in subject diffusion space
            to create slf2_ant
Input:      frontal_mid and ba6and8 in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/slf2_ant_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildSlf2Ant_L ()
{
  Stand2Diff frontal_mid_l ba6and8_l

  fslmaths ${output_dir}/${subj}/subjectrois/frontal_mid_l_diff_bin -add ${output_dir}/${subj}/subjectrois/ba6and8_l_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/slf2_ant_l_diff_bin -odt input

  TrimEndpoints slf2_ant_l_diff_bin
}

#==============================================================================
:
: <<COMMENTBLOCK

Function:   BuildSlf2Ant_R
Purpose:    Add frontal_mid and ba6and8 in subject diffusion space
            to create slf2_ant
Input:      frontal_mid and ba6and8 in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/slf2_ant_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildSlf2Ant_R ()
{
  Stand2Diff frontal_mid_r ba6and8_r

  fslmaths ${output_dir}/${subj}/subjectrois/frontal_mid_r_diff_bin -add ${output_dir}/${subj}/subjectrois/ba6and8_r_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/slf2_ant_r_diff_bin -odt input

  TrimEndpoints slf2_ant_r_diff_bin
}
#==============================================================================
: <<COMMENTBLOCK

Function:   BuildWernickes_L
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion
            space to create Wernickes5
Input:      angular, mtl_post, stg, supramarginal in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/wernickes5_l_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildWernickes_L ()
{
  Stand2Diff angular_l mtg_post_l stg_l supramarginal_l

  fslmaths ${output_dir}/${subj}/subjectrois/angular_l_diff_bin -add ${output_dir}/${subj}/subjectrois/mtg_post_l_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/stg_l_diff_bin -add ${output_dir}/${subj}/subjectrois/supramarginal_l_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/wernickes5_l_diff_bin -odt input

  TrimEndpoints wernickes5_l_diff_bin
}

#==============================================================================
: <<COMMENTBLOCK

Function:   BuildWernickes_R
Purpose:    Add angular, mtl_post, stg, supramarginal in subject diffusion
            space to create Wernickes5
Input:      angular, mtl_post, stg, supramarginal in subject diffusion space
Output:     ${output_dir}/${subj}/subjectrois/wernickes5_r_diff
Calls:      Stand2Diff and TrimEndpoints in this script

COMMENTBLOCK

BuildWernickes_R ()
{
  Stand2Diff angular_r mtg_post_r stg_r supramarginal_r

  fslmaths ${output_dir}/${subj}/subjectrois/angular_r_diff_bin -add ${output_dir}/${subj}/subjectrois/mtg_post_r_diff_bin \
  -add ${output_dir}/${subj}/subjectrois/stg_r_diff_bin -add ${output_dir}/${subj}/subjectrois/supramarginal_r_diff_bin \
  -bin ${output_dir}/${subj}/subjectrois/wernickes5_r_diff_bin -odt input

  TrimEndpoints wernickes5_r_diff_bin
}


#==============================================================================

: <<COMMENTBLOCK

Function:   Stand2Diff
Purpose:    Convert standard space roi to diffusion space
Input:      Standard space roi in ${ROIS}
Output:     binary roi in subject diffusion space
Calls:      MNI2B0MaskWarp from bip_functions.sh

COMMENTBLOCK

Stand2Diff ()
{
  for roi in $@; do
    if [ ! -e ${output_dir}/${subj}/subjectrois/${roi}_diff_bin.nii.gz ]; then
      MNI2B0MaskWarp ${BIP_HOME}/REF/IMAGES/ROIS/${roi} ${output_dir}/${subj}/subjectrois/${roi}_diff
    else
      echo "${roi}_diff already exists"
    fi
  done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   TrimEndpoints
Purpose:    Trim a diff space roi with the csf_diff mask
Input:      roi_diff
Output:     roi_csf
Calls:      None

COMMENTBLOCK

TrimEndpoints ()
{
  for roi in $@; do
    fslmaths ${output_dir}/${subj}/subjectrois/${roi} -sub ${output_dir}/${subj}/subjectrois/${subj}_B0_csfseg_bin.nii.gz -thr 0 -bin ${output_dir}/${subj}/subjectrois/${roi}_csf -odt input
    imrm ${output_dir}/${subj}/subjectrois/${roi}
  done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help if user enters fewer than one argument
Input:      None
Output:     Help Message
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
  echo "Prepare a subject directory for running bip scripts:"
  echo "Generate roi masks for each endpoint."
  echo "Generate an inverse mask to restrict the path of each tract."
  echo "Generate the tract bip directory in the dwi dir , e.g., arc_l_bip"
  echo "Possible tracts are:
    arc_l
    arc_r
    aslant_l
    aslant_r
    b3tob3_ih
    cb2th_lr
    cb2th_rl
    cst_l
    cst_r
    extcap_l
    extcap_r
    fr2th_l
    fr2th_r
    ilf_l
    ilf_r
    iof_l
    iof_r
    mdlf_l
    mdlf_r
    par2th_l
    par2th_r
    slf2_l
    slf2_r
    tmp2th_l
    tmp2th_r
    unc_l
    unc_r
    vwfa2vwfa_ih
    w5tow5_ih"
  echo "======================="
  echo "example: bip_prep.sh arc_l 327"
  echo "will run bip_prep.sh for the left arcuate tract and sub-327"
  exit 1
}

#==============================================================================
: <<COMMENTBLOCK

Function:   arc_l
Purpose:    prepare for running the left arcuate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

arc_l ()
{
  BuildBrocas_L
  BuildWernickes_L
  BuildInverse arc_l
  BuildDir arc_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   arc_r
Purpose:    prepare for running the right arcuate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

arc_r ()
{
  BuildBrocas_R
  BuildWernickes_R
  BuildInverse arc_r
  BuildDir arc_r
}
#==============================================================================
: <<COMMENTBLOCK

Function:   aslant_l
Purpose:    prepare for running the left aslant tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

aslant_l ()
{
  BuildBrocasClassic_L
  BuildEndpoints sma_l
  BuildInverse aslant_l
  BuildDir aslant_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   aslant_r
Purpose:    prepare for running the right aslant tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

aslant_r ()
{
  BuildBrocasClassic_R
  BuildEndpoints sma_r
  BuildInverse aslant_r
  BuildDir aslant_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   b3tob3_ih
Purpose:    prepare for running the trans-callosal tract between brocas on the right and left.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

b3tob3_ih ()
{
  BuildBrocas_L
  BuildBrocas_R
  BuildInverse b3tob3_ih
  BuildDir b3tob3_ih
}

#==============================================================================
: <<COMMENTBLOCK

Function:   cb2th_lr
Purpose:    prepare for running the tract between the left cerebellar hemisphere
            and the contralateral thalamus.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

cb2th_lr ()
{
  BuildEndpoints cb_l thalamus_r
  BuildInverse cb2th_lr
  BuildDir cb2th_lr
}

#==============================================================================
: <<COMMENTBLOCK

Function:   cb2th_rl
Purpose:    prepare for running the tract between the right cerebellar hemisphere
            and the contralateral thalamus.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

cb2th_rl ()
{
  BuildEndpoints cb_r thalamus_l
  BuildInverse cb2th_rl
  BuildDir cb2th_rl
}

#==============================================================================
: <<COMMENTBLOCK

Function:   cst_l
Purpose:    prepare for running the tract between the motor-sensory region and
            and the brainstem.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

cst_l ()
{
  BuildEndpoints motor-sensory_l brainstem_all
  BuildInverse cst_l
  BuildDir cst_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   cst_r
Purpose:    prepare for running the tract between the motor-sensory region and
            and the brainstem.
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

cst_r ()
{
  BuildEndpoints motor-sensory_r brainstem_all
  BuildInverse cst_r
  BuildDir cst_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   extcap_l
Purpose:    prepare for running the left extreme capsule tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

extcap_l ()
{
  BuildBrocas_L
  BuildWernickes_L
  BuildInverse extcap_l
  BuildDir extcap_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   extcap_r
Purpose:    prepare for running the right extreme capsule tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

extcap_r ()
{
  BuildBrocas_R
  BuildWernickes_R
  BuildInverse extcap_r
  BuildDir extcap_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   fr2th_l
Purpose:    prepare for running the tract between the frontal lobe
            and thalamus on the left.
Input:      Assumes the existence of registration and segmentation files.
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

fr2th_l ()
{
  BuildEndpoints fr_l thalamus_l
  BuildInverse fr2th_l
  BuildDir fr2th_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   fr2th_r
Purpose:    prepare for running the tract between the frontal lobe
            and thalamus on the right.
Input:      Assumes the existence of registration and segmentation files.
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

fr2th_r ()
{
  BuildEndpoints fr_r thalamus_r
  BuildInverse fr2th_r
  BuildDir fr2th_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   ilf_l
Purpose:    prepare for running the left inferior longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

ilf_l ()
{
  BuildATL_L
  BuildEndpoints vwfa_l
  BuildInverse ilf_l
  BuildDir ilf_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   ilf_r
Purpose:    prepare for running the right inferior longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

ilf_r ()
{
  BuildATL_R
  BuildEndpoints vwfa_r
  BuildInverse ilf_r
  BuildDir ilf_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   iof_l
Purpose:    prepare for running the left inferior occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

iof_l ()
{
  BuildEndpoints frontal_orbital_l occipital_l
  BuildInverse iof_l
  BuildDir iof_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   iof_r
Purpose:    prepare for running the right inferior occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

iof_r ()
{
  BuildEndpoints frontal_orbital_r occipital_r
  BuildInverse iof_r
  BuildDir iof_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   mdlf_l
Purpose:    prepare for running the left middle longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

mdlf_l ()
{
  BuildATL_L
  BuildMdlfPost_L
  BuildInverse mdlf_l
  BuildDir mdlf_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   mdlf_r
Purpose:    prepare for running the right middle longitudinal fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

mdlf_r ()
{
  BuildATL_R
  BuildMdlfPost_R
  BuildInverse mdlf_r
  BuildDir mdlf_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   par2th_l
Purpose:    prepare for running the tract between the parietal lobe
            and thalamus on the left.
Input:      Assumes the existence of registration and segmentation files.
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

par2th_l ()
{
  BuildEndpoints par_l thalamus_l
  BuildInverse par2th_l
  BuildDir par2th_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   par2th_r
Purpose:    prepare for running the tract between the parietal lobe
            and thalamus on the right.
Input:      Assumes the existence of registration and segmentation files.
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

par2th_r ()
{
  BuildEndpoints par_r thalamus_r
  BuildInverse par2th_r
  BuildDir par2th_r
}
#==============================================================================
: <<COMMENTBLOCK

Function:   slf2_l
Purpose:    prepare for running the left superior longitudinal fasciculus part 2 tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

slf2_l ()
{
  BuildSlf2Ant_L
  BuildEndpoints angular_l
  BuildInverse slf2_l
  BuildDir slf2_l
}


#==============================================================================
: <<COMMENTBLOCK

Function:   slf2_r
Purpose:    prepare for running the right superior longitudinal fasciculus part 2 tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

slf2_r ()
{
  BuildSlf2Ant_R
  BuildEndpoints angular_r
  BuildInverse slf2_r
  BuildDir slf2_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   tmp2th_l
Purpose:    prepare for running the tract between the temporal lobe
            and thalamus on the left.
Input:      Assumes the existence of registration and segmentation files.
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

tmp2th_l ()
{
  BuildEndpoints tmp_l thalamus_l
  BuildInverse tmp2th_l
  BuildDir tmp2th_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   tmp2th_r
Purpose:    prepare for running the tract between the parietal lobe
            and thalamus on the right.
Input:      Assumes the existence of registration and segmentation files.
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

tmp2th_r ()
{
  BuildEndpoints tmp_r thalamus_r
  BuildInverse tmp2th_r
  BuildDir tmp2th_r
}
#==============================================================================
: <<COMMENTBLOCK

Function:   unc_l
Purpose:    prepare for running the left uncinate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

unc_l ()
{
  BuildATL_L
  BuildEndpoints frontal_orbital_l
  BuildInverse unc_l
  BuildDir unc_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   unc_r
Purpose:    prepare for running the right uncinate tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

unc_r ()
{
  BuildATL_R
  BuildEndpoints frontal_orbital_r
  BuildInverse unc_r
  BuildDir unc_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   vof_l
Purpose:    prepare for running the left vertical occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

vof_l ()
{
  BuildEndpoints angular_l vwfa_l
  BuildInverse vof_l
  BuildDir vof_l
}

#==============================================================================
: <<COMMENTBLOCK

Function:   vof_r
Purpose:    prepare for running the right vertical occipital fasciculus tract
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

vof_r ()
{
  BuildEndpoints angular_r vwfa_r
  BuildInverse vof_r
  BuildDir vof_r
}

#==============================================================================
: <<COMMENTBLOCK

Function:   vwfa2vwfa_ih
Purpose:    prepare for running the trans-callosal tract between the visual
            word form areas on the left and right
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

vwfa2vwfa_ih ()
{
  BuildEndpoints vwfa_l vwfa_r
  BuildInverse vwfa2vwfa_ih
  BuildDir vwfa2vwfa_ih
}

#==============================================================================
: <<COMMENTBLOCK

Function:   w5tow5_ih
Purpose:    prepare for running the trans-callosal tract between wernicke's area
            on the left and right
Input:      Assumes the existence of registration and segmentation files.
            check.sh reg must have been run
Output:     rois in subjectrois, including the inverse mask, the endpoints
            and the rois that make up each endpoint.
            A directory under derivatives/dwi where bip will run.
Calls:      Several functions in this script.

COMMENTBLOCK

w5tow5_ih ()
{
  BuildWernickes_L
  BuildWernickes_R
  BuildInverse w5tow5_ih
  BuildDir w5tow5_ih
}

#==============================================================================


: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Print relevant help message
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
  if [ $# -lt 2 ]; then
    echo "Usage: $0 <tract> <subjectnum>"
    echo "Example: $0 arc_r 327"
    exit 1
  fi

  echo "================================ "
}


#==============================================================================
: <<COMMENTBLOCK

Function:   Main
Purpose:    Run the correct set of functions with appropriate arguments
Input:      Assumes existence of a bunch of files the functions need
Output:     Subject directory prepared for running bip scripts.

COMMENTBLOCK

Main ()
{
  ${tract} ${subjectnum}
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

source ${BIP_HOME}/bip_functions.sh

for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
  esac                                          # End case
done

# We pass 4 arguments from run.py: the subjectnum, bids_dir and output_dir, tract
subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
tract=$4

tract_dir=${output_dir}/${subj}/dwi/${tract}_bip

Main
