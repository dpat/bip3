#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

###############################################################################

###############################################################################

COMMENTBLOCK
###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   Main
Purpose:    Create stats files with header information in the output directory
Input:      *endpoint* and *tract* tsv files in each subject's stats directory
Output:     ${output_dir}/all_stats_endpoints.tsv and
            ${output_dir}/all_stats_tracts.tsv

COMMENTBLOCK

Main ()
{
  if [ ! -e ${output_dir}/all_stats_endpoints.tsv ]; then
    echo "Create necessary stats files to hold concatenated subject values:"
    echo "${output_dir}/all_stats_endpoints.tsv"

    printf  "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "subject" "tract" "side" "end" "volmm2" "COG_x" "COG_y" "COG_z" > ${output_dir}/all_stats_endpoints.tsv

  fi

  if [ ! -e ${output_dir}/all_stats_tracts.tsv ]; then
    echo "Create necessary stats files to hold concatenated subject values:"
    echo "${output_dir}/all_stats_tracts.tsv"

    printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" "subject" "tract" "side" "volmm2" "MD" "FA" "RD" "PD" "MO" > ${output_dir}/all_stats_tracts.tsv
  fi
}
#==============================================================================

# We pass 1 arguments from run.py: the output_dir
output_dir=$1
###################
echo "output_dir = ${output_dir}"

if [ $# -lt 1 ]
then
  HelpMessage
  exit 1
fi

Main ${output_dir}
