#!/usr/bin/env python3
from glob import glob
import sys

def main(argv):
    bpath = "{0}/{1}".format(argv[1], argv[2])
    bfiles = glob(bpath)
    print(len(bfiles))

if __name__ == "__main__":
    main(sys.argv)
