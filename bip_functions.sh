#!/bin/bash

#==============================================================================
: <<COMMENTBLOCK

Function:   Binarize
Purpose:    Threshold a continuous image to make it a 1-0 binary mask instead
Input:     	2 arguments: input file, output file
Output:     1-0 mask of input image

COMMENTBLOCK

Binarize ()
{
  output=$1
  fslmaths ${output} -thr 0.5 -bin ${output}_bin -odt char
  imrm ${output}
}

#==============================================================================
: <<COMMENTBLOCK

Function:   B02MNIWarp
Purpose:    applywarp creates B0 image in MNI space.  Do NOT apply to vector images.
Input:     	2 arguments: input file, output file
Output:     fnirt warped DTI image in MNI space
Caller:     Bip_stats

COMMENTBLOCK

B02MNIWarp ()
{
  input=$1
  output=$2
  dwi_outdir=${bids_dir}/derivatives/fsl_dwi_proc
  applywarp -r ${FSLDIR}/data/standard/MNI152_T1_2mm -i ${input} --out=${output}  --warp=${dwi_outdir}/${subj}/reg/${subj}_b0_mni_warp
}

#==============================================================================
: <<COMMENTBLOCK

Function:   B02MNIMaskWarp
Purpose:    applywarp creates B0 image in MNI space.  Do NOT apply to vector images.
Caller: 		CheckReg function called by check.sh
Input:     	2 arguments: input file, output file
Output:     fnirt warped DTI image in MNI space

COMMENTBLOCK

B02MNIMaskWarp ()
{
  input=$1
  output=$2
  B02MNIWarp ${input} ${output}
  Binarize ${output}
}

#==============================================================================

: <<COMMENTBLOCK

Function:   CheckDir
Purpose:    Determine whether directory needed for processing is present. Produce
            messages to standard output, and an error file if there are problems and processing should not proceed.
Input:      directory name, success_msg, fail_msg
Output:     Information. If there is a problem, then an error file is generated.
            Function returns "True" if the directory exists, "False" otherwise.
Caller:     CheckFilesSetup, CheckFilesPrep, CheckFilesBip
Calls:      From bip_functions: calls DirExists and ultimately dir_exist.py

COMMENTBLOCK

CheckDir ()
{
  dir_name=$1
  dir_success_msg=$2
  dir_fail_msg=$3
  err_txt=${bids_dir}/${subj}_error.txt

  dir_exist=$(DirExist $dir_name)

  if [ "${dir_exist}" = "False" ]; then
    if [ ! -e ${err_txt} ]; then
      touch ${err_txt}
      echo "===========================================" >> ${err_txt}
      echo ${subj} >> ${err_txt}
    fi
    echo "${dir_fail_msg}"
    echo "${dir_fail_msg}" >> ${err_txt}
  else
    echo "${dir_success_msg}"
  fi
}


#==============================================================================

: <<COMMENTBLOCK

Function:   CheckFiles
Purpose:    Determine whether files needed for processing are present. Produce
            messages to standard output, and an error file if there are problems
            and processing should not proceed.
Input:      directory name, file name, success_msg, fail_msg
Output:     Information.  If there is a problem, then an error file is generated.
            Function returns "True" if the file pattern exists, "False" otherwise.
Caller:     CheckFilesSetup, CheckFilesPrep, CheckFilesBip
Calls:      From bip_functions: calls FileExists and or CountFiles

COMMENTBLOCK

CheckFiles ()
{
  dir_name=$1
  file_name=$2
  success_msg=$3
  fail_msg=$4
  err_txt=${bids_dir}/${subj}_error.txt

  local exists=$(FileExists $dir_name $file_name)
  if [ "$exists" = "0" ]
  then
    echo "${success_msg}"
  else
    echo "${fail_msg}"
    if [ ! -e ${err_txt} ]; then
      touch ${err_txt}
      echo "===========================================" >> ${err_txt}
      echo "${subj}" >> ${err_txt}
    else
      echo "${fail_msg}"
    fi
    echo "${fail_msg}" >> ${err_txt}
  fi
}

#==============================================================================
: <<COMMENTBLOCK

Function:   CountFiles
Purpose:    To count how many files meet criterion by calling file_counter.py
Input:      Two arguments: directory and file pattern
Output:     A count of how many files meet the criterion
Caller:     CheckFiles calls this

COMMENTBLOCK

CountFiles ()
{
  local FILE_COUNT=$(file_counter.py $1 $2)
  echo $FILE_COUNT
}

#==============================================================================
: <<COMMENTBLOCK

Function:   DirExist
Purpose:    To determine whether a directory exists that meet criterion
Input:      directory pattern
Output:     True if the directory exists, else False
Caller:     CheckFiles calls this

COMMENTBLOCK

DirExist ()
{
  dir_exist=$(dir_exist.py $1)
  echo "${dir_exist}"
}

#==============================================================================
: <<COMMENTBLOCK

Function:   FileExists
Purpose:    To determine whether any files exist that meet criterion
Input:      Two arguments: directory and file pattern
Output:     0 if the test is successful and the file exists, else 1 if the file does not exist.
Caller:     CheckFiles calls this

COMMENTBLOCK

FileExists ()
{
  local cnt=$(CountFiles $1 $2)
  if [ "$cnt" -gt 0 ]; then    # some files exist
  echo 0
else                    # no files exist
  echo 1
fi
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkSublistArray
Purpose:    Extract a sublist from a larger list.
            The sublist is fed to another process. Put the
            sublist into a bash array so we can access all the items. This is
            useful for accessing a set of images for operations. It is useful
            because it allows you to store a bunch of small lists in one giant
            list which is tidier and easier to keep track of.
Input:			1) The name of a list containing sublists of the following form:
            --animal_start--
            anteater
            bull
            dog
            elephant
            --animal_end--
            2) The delimiter of the sublist of interest (e.g., animal)
Output:     An array containing the sublist

COMMENTBLOCK

MkSublistArray ()
{
  if [ $# -lt 2 ]
  then
    echo "You need 2 arguments, the sourcelist and the sublist"
    echo "Example: $0 lst_tract_seed_target_masks.txt arc5_l"
    exit 1
  fi

  sourcelist=$1
  sublist=$2
  LISTS=${BIP_HOME}/REF/LISTS

  # Find start and end of sublist with awk
  # Remove sublist delimiters
  # Results-->clean_sublist
  clean_sublist=$(awk /${sublist}_start/,/${sublist}_end/ ${LISTS}/${sourcelist} | sed '/^--/d')

  i=1                                           # set i=1

  for line in ${clean_sublist} ; do             # for each line in the clean_sublist
  sublist_array[$i]=${line}                     # line-->element in the array "sublist_array"
  let "i= ${i} + 1"                             # increment i
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MNI2B0MaskWarp
Purpose:    applywarp creates the B0 space mask image from an MNI image.
Caller: 		Stand2Diff in bip_prep.sh
Input:     	1 arguments: input file
Output:     fnirt warped MNI space mask image in DWI_DERIV space
Notes: 			Modified for bipbids
Calls:			Binarize in bip_functions.sh

COMMENTBLOCK

MNI2B0MaskWarp ()
{
  # Using this specialized output dir for dwi processing
  fsl_dwi_proc=${bids_dir}/derivatives/fsl_dwi_proc
  if [ $# -eq 2 ]
  then
    input=$1
    output=$2
  elif [ $# -eq 1  ]; then
    input=$1
    output=${input}_B0
  fi

  applywarp -r ${fsl_dwi_proc}/${subj}/dwi/nodif_brain.nii.gz -i ${input}  --out=${output}  --warp=${fsl_dwi_proc}/${subj}/reg/${subj}_mni_b0_warp
  Binarize ${output}
}

#==============================================================================
: <<COMMENTBLOCK

Function:   STR2B0Mask
Purpose:    Creates B0 space mask from T1w image with linear registration.
            Comparison to the warped registration clearly shows the linear one to be superior.
Caller: 		MkSeg function called by prep.sh
Input:      1 argument required: input file; 1 optional argument: output file
Output:     flirt warped T1w space mask image in B0 space
Notes: 			Updated for bipbids

COMMENTBLOCK

STR2B0Mask ()
{
  if [ $# -eq 2 ]
  then
    input=$1
    output=$2
  elif [ $# -eq 1  ]; then
    input=$1
    output=${input}_B0
  fi

  ref_image=$(${output_dir}/${subj}/dwi/nodif_brain.nii.gz)

  flirt -in ${input} -datatype float -ref ${ref_image} -init ${output_dir}/${subj}/reg/${subj}_str_b0.mat -applyxfm -out ${output}

  Binarize ${output}
}
